import hashlib
import os
from datetime import datetime
from operator import itemgetter
from pathlib import Path

import sqlalchemy

import app.db_models
from app.auth import get_user
from app.db_connect import SessionLocal
from app.db_models import Todo, Xlsx, Tags
from app.images import get_image
from app.shemas import CreateTodo, UpdateTodo
import string
import random
import matplotlib as mpl
import matplotlib.pyplot as plt
import matplotlib.dates as mdates
import datetime as dt
import csv
import xlsxwriter
import openpyxl
import gitlab

private_token = 'glpat-sXCspwszzk9bQrJC6xQC'


def create_todo(todo: CreateTodo):
    db = SessionLocal()
    owner = get_user(id=todo.owner_id)
    image = get_image(id=todo.image_id)
    td = Todo(title=todo.title,
              details=todo.details,
              tag=todo.tag,
              completed=todo.completed,
              origin=todo.origin,
              completed_date=todo.completed_date,
              created_date=todo.created_date,
              owner_id=todo.owner_id,
              image_id=todo.image_id,
              image=image,
              owner=owner)

    db.add(td)
    db.commit()
    db.refresh(td)
    db.close()
    return td


def generate_todo(todo: CreateTodo):
    db = SessionLocal()
    owner = get_user(id=todo.owner_id)
    td = Todo(title=todo.title,
              details=todo.details,
              tag=todo.tag,
              completed=False,
              origin=todo.origin,
              owner_id=todo.owner_id,
              created_date=datetime.now().strftime('%Y-%m-%d'),
              owner=owner)
    db.add(td)
    db.commit()
    db.refresh(td)
    db.close()


def get_todos():
    db = SessionLocal()
    todos = db.query(app.db_models.Todo).all()
    for todo in todos:
        f = todo.image
        f = todo.owner
    db.close()
    return todos


def get_todo(id: int):
    db = SessionLocal()
    todo = db.query(app.db_models.Todo).filter(app.db_models.Todo.id == id).first()
    db.close()
    return todo


def delete_todo(id):

    try:
        db = SessionLocal()
        todo = get_todo(id)
        db.delete(todo)
        db.commit()
        db.close()
    except Exception as e:
        return e
    return todo


def update_todo(todo: UpdateTodo):
    db = SessionLocal()
    update_file = db.query(app.db_models.Todo).filter(app.db_models.Todo.id == todo.id).first()
    update_file.title = todo.title
    update_file.details = todo.details
    update_file.tag = todo.tag
    if todo.completed:
        update_file.completed_date = datetime.now().strftime('%Y-%m-%d')
    else:
        update_file.completed_date = None
    update_file.completed = todo.completed
    if todo.image_id:
        update_file.image_id = todo.image_id
    db.commit()
    db.refresh(update_file)
    db.close()
    return todo


def gen(id,
        count):
    letters = string.ascii_lowercase
    list_tag = ["studies", "personal", "plans", "empty"]
    db = SessionLocal()
    for i in range(count):
        size = random.randint(5, 20)
        title = ''.join(random.choice(letters) for j in range(size))
        details = ''.join(random.choice(letters) for k in range(size))
        tag = list_tag[random.randint(0, 3)]
        print(title, details, tag)
        generate_todo(CreateTodo(title=title,
                  details=details,
                  tag=tag,
                  completed=False,
                  owner_id=id,
                  created_date=datetime.now().strftime('%d-%m-%Y'),
                  origin="generated",
                  ))


def vis(logger):
    db = SessionLocal()
    todos = db.query(app.db_models.Todo).all()
    db.close()
    data_values = [0, 0, 0, 0]
    for i in todos:
        logger.debug(f"todo tag = {i.tag}")
        if i.tag == Tags.studies:
            logger.debug("Hello Tags.studies")
            data_values[0] += 1
        if i.tag == Tags.personal:
            data_values[1] += 1
        if i.tag == Tags.plans:
            data_values[2] += 1
        if i.tag == Tags.empty:
            data_values[3] += 1
    return data_values


def export_xlsx():
    db = SessionLocal()
    todos = db.query(app.db_models.Todo).all()
    db.close()
    workbook = xlsxwriter.Workbook('todo.xlsx')
    worksheet = workbook.add_worksheet()
    todo = todos[0]
    lst = app.db_models.Todo.attr(todo)
    for i, key in enumerate(lst):
        worksheet.write(0, i, key)
    for i, todo in enumerate(todos):
        for j in range(8):
            if j == 0:
                worksheet.write(i + 1, j, todo.id)
            elif j == 1:
                worksheet.write(i + 1, j, todo.title)
            elif j == 2:
                worksheet.write(i + 1, j, todo.details)
            elif j == 3:
                worksheet.write(i + 1, j, str(todo.tag).split(".")[1])
            elif j == 4:
                worksheet.write(i + 1, j, str(todo.origin).split(".")[1])
            elif j == 5:
                worksheet.write(i + 1, j, todo.completed)
            elif j == 6:
                worksheet.write(i + 1, j, todo.created_date)
            elif j == 7:
                worksheet.write(i + 1, j, todo.completed_date)
    workbook.close()


def import_xlsx(filename,
                id
                ):
    workbook = openpyxl.load_workbook(filename)

    sheet = workbook.active
    data_dict = {}

    for row in sheet.iter_rows(values_only=True):
        column_name = row[0]
        column_data = row[1:]
        data_dict[column_name] = column_data
    data_dict.pop('id')
    for key in data_dict.keys():
        if data_dict[key][4] == "=FALSE()":
            data_dict[key][4] = 0
        elif data_dict[key][4] == "=TRUE()":
            data_dict[key][4] = 1
        todo = CreateTodo(title=data_dict[key][0],
                          owner_id=id,
                          details=data_dict[key][1],
                          tag=data_dict[key][2],
                          origin=data_dict[key][3],
                          completed=data_dict[key][4],
                          completed_date=data_dict[key][5],
                          created_date=data_dict[key][6],
                          )
        create_todo(todo)
    db = SessionLocal()
    xl = Xlsx(filename=filename)
    db.add(xl)
    db.commit()
    db.refresh(xl)
    db.close()
    os.remove(filename)
    return data_dict


def complete_todo(id: int):
    db = SessionLocal()
    update_file = db.query(app.db_models.Todo).filter(app.db_models.Todo.id == id).first()
    if not update_file.completed:
        update_file.completed_date = datetime.now().strftime('%Y-%m-%d')
    else:
        update_file.completed_date = None
    update_file.completed = not update_file.completed
    db.commit()
    db.refresh(update_file)
    db.close()


def get_xlsx():
    db = SessionLocal()
    xls = db.query(app.db_models.Xlsx).all()
    print(xls)
    db.close()
    return xls

def git_create(id: int,
               pth = "https://gitlab.com/14NAGIBATOR88/fastapi-postgres-docker",
               private_token = 'glpat-sXCspwszzk9bQrJC6xQC'):
    vcs = pth[:19:]
    gl = gitlab.Gitlab(url=vcs, private_token=private_token)

    project_url = pth[19::]
    project = gl.projects.get(project_url)
    issues = project.issues.list()
    #print(issues)
    for issue in issues:
        todo = CreateTodo(title=issue.title,
                          details=issue.description,
                          origin="created",
                          tag="empty",
                          owner_id=id,
                          created_date=issue.created_at[0:10])
        create_todo(todo)
        return todo


def delete_all():
    todos = get_todos()
    for todo in todos:
        delete_todo(todo.id)

def delete_todos(start, stop):
    db = SessionLocal()
    files = db.query(app.db_models.Todo).filter(app.db_models.Todo.id <= stop).all()
    db.close()
    for file in files:
        #print(file.id)
        if file.id >= start:
            delete_todo(file.id)
        else:
            files.pop(file.id)
    return files


def todo_range(start: str):
    db = SessionLocal()
    files = db.query(app.db_models.Todo).filter(app.db_models.Todo.created_date >= start).all()
    db.close()
    return files

def test():
    db = SessionLocal()
    file = db.query(app.db_models.Todo).filter(app.db_models.Todo.id == 2).first()
    #image = file.image
    db.commit()
    f = file.image
    db.close()
    #f = file.image

    return file.image

def get_self_todos():
    db = SessionLocal()
    todos = db.query(app.db_models.Todo).filter(app.db_models.Todo.tag == 'personal').all()
    db.close()
    return todos


def get_plans_todos():
    db = SessionLocal()
    todos = db.query(app.db_models.Todo).filter(app.db_models.Todo.tag == 'plans').all()
    db.close()
    return todos


def get_teach_todos():
    db = SessionLocal()
    todos = db.query(app.db_models.Todo).filter(app.db_models.Todo.tag == 'studies').all()
    db.close()
    return todos