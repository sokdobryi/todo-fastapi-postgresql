import enum
from datetime import datetime

from sqlalchemy import Column, Integer, String, null, Enum, Boolean, DateTime, ForeignKey, MetaData
from sqlalchemy.orm import relationship

from app.db_connect import Base

class Tags(enum.Enum):
    studies = "studies"
    personal = "personal"
    plans = "plans"
    empty = "empty"


class Origins(enum.Enum):
    generated = "generated"
    created = "created"


class Todo(Base):
    """Todo model
    """
    __tablename__ = 'todos'
    id = Column(Integer, primary_key=True)
    title = Column(String)
    details = Column(String, nullable=True, default=None)
    tag = Column(Enum(Tags), default="empty")
    origin = Column(Enum(Origins))
    completed = Column(Boolean, default=False)
    created_date = Column(String)
    completed_date = Column(String, nullable=True, default=None)

    image_id = Column(Integer, ForeignKey("images.id"), nullable=True, default=None)
    owner_id = Column(Integer, ForeignKey("users.id"))

    owner = relationship('User', backref='owner_posts', foreign_keys=[owner_id], lazy="joined")
    image = relationship('Image', backref='owner_posts', foreign_keys=[image_id], lazy="joined")


    def __repr__(self):
        return f'<Todo {self.id}>'

    # def attr(self):
    #     return["id", "title", "details", "tag", "origin", "completed", "created_date", "completed_date", "image"]


class User(Base):
    __tablename__ = "users"

    id = Column(Integer, primary_key=True, index=True)
    username = Column(String, unique=True, index=True, nullable=True, default=null())
    hashed_password = Column(String, nullable=True, default=null())



class Image(Base):
    __tablename__ = "images"

    id = Column(Integer, primary_key=True, index=True)
    path = Column(String, unique=True, index=True, nullable=True, default="./static/img/todos/gitlab.png")
    hashed_image = Column(String, nullable=True, default=null())





class Xlsx(Base):
    __tablename__ = "xlsx"

    id = Column(Integer, primary_key=True, index=True)
    filename = Column(String, unique=False, index=True, nullable=True, default=null())
