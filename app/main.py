import os
import logging
from datetime import timedelta

import openpyxl
from fastapi.security import OAuth2PasswordRequestForm
from starlette.status import HTTP_302_FOUND

import app.db_models

from typing import Optional
from starlette import status
from starlette.responses import FileResponse, JSONResponse
from starlette.staticfiles import StaticFiles
from starlette.templating import Jinja2Templates
from fastapi import FastAPI, Request, Response, UploadFile, File, Depends, Form, status, Header, Body, HTTPException
from fastapi.templating import Jinja2Templates
from fastapi.responses import RedirectResponse
from fastapi.staticfiles import StaticFiles

from app.auth import get_user, create_user, get_current_user, Token, authenticate_user, ACCESS_TOKEN_EXPIRE_MINUTES, \
    create_access_token
from app.db_connect import engine
from app.images import create_images, get_checksum, images
from app.todos import *
from app.shemas import CreateTodo, UpdateTodo, CreateImage

UPLOADED_IMG_PATH = "./static/img/todos"

server = FastAPI()
app.db_models.Base.metadata.create_all(engine)
os.chdir("app")

logger = logging.getLogger(__name__)
logger.setLevel(logging.DEBUG)
handler = logging.FileHandler(filename="../main.log")
logger.addHandler(handler)
handler.setFormatter(logging.Formatter(fmt='[%(asctime)s: %(levelname)s] %(message)s'))

templates = Jinja2Templates(directory="templates")
server.mount("/app/static", StaticFiles(directory="static"), name="static")


@server.get("/")
async def home(request: Request):
    """
    Function for main page
    :param request: request
    :return: HTML of main page 
    """
    logger.info(f'client=\"{request.client.host}:{request.client.port}\" url=\"/\" status=\"HTTP_200_OK\"')
    todos = get_todos()[::-1]
    count_of_todos = len(todos)
    return templates.TemplateResponse("main.html", {"request": request,
                                                    "todos": todos,
                                                    "count_of_todos": count_of_todos})


@server.get("/list")
async def home(request: Request):
    """
    Function for main page
    :param request: request
    :return: HTML of main page
    """
    logger.info(f'client=\"{request.client.host}:{request.client.port}\" url=\"/\" status=\"HTTP_200_OK\"')
    todos = get_todos()[::-1]
    return todos


@server.post("/add")
async def todo_add(request: Request,
                   token: str = Form(...),
                   title: str = Form(...),
                   details: Optional[str] = Form(None),
                   tag: str = Form("empty"),
                   foto: Optional[UploadFile] = File(None)):
    """
    Function for adding new todo
    :param token: access token
    :param request: request
    :param title: title of new todo
    :param details: description of todo
    :param tad: tag of new news
    :param foto: img for todos
    :return: redirect to main page
    """
    user = get_current_user(token)
    logger.info(f'client=\"{request.client.host}:{request.client.port}\" start creating post')
    try:
        path = f'{UPLOADED_IMG_PATH}/{foto.filename}'
        with open(path, "wb") as uploaded_file:
            file_content = await foto.read()
            uploaded_file.write(file_content)
            uploaded_file.close()
        a = get_checksum(path)
        img = create_images(CreateImage(path=path, hashed_image=get_checksum(path)))
        model = CreateTodo(title=title,
                           details=details,
                           tag=tag,
                           origin="created",
                           owner_id=user.id,
                           image_id=img.id)

    except:
        model = CreateTodo(title=title,
                           details=details,
                           tag=tag,
                           origin="created",
                           owner_id=user.id,
                           # image=img.id)
                           )

    todo = create_todo(model)
    # return todo
    return RedirectResponse(url=server.url_path_for("home"), status_code=status.HTTP_303_SEE_OTHER)


@server.post("/edit/{todo_id}")
async def todo_edit(request: Request,
                    todo_id: int,
                    title: str = Form(...),
                    details: str = Form(...),
                    tag: str = Form(...),
                    completed: bool = Form(False),
                    token: str = Form(...),
                    foto: Optional[UploadFile] = File(None)):
    """
    Function for edding todo
    :param request: request
    :param todo_id: id of todo
    :param title: title of todo
    :param details: description of todo
    :param tag: tag of todo
    :param completed: info if complited todo
    :return: redirect to main page
    """
    user = get_current_user(token)
    if not get_todo(todo_id).owner_id == user.id:
        return "You have not permisson"
    try:
        path = f'{UPLOADED_IMG_PATH}/{foto.filename}'
        with open(path, "wb") as uploaded_file:
            file_content = await foto.read()
            uploaded_file.write(file_content)
            uploaded_file.close()
        a = get_checksum(path)
        img = create_images(CreateImage(path=path, hashed_image=get_checksum(path)))
        model = UpdateTodo(id=todo_id,
                           title=title,
                           details=details,
                           tag=tag,
                           origin="created",
                           owner_id=user.id,
                           image_id=img.id,
                           completed=completed)

    except:
        model = UpdateTodo(id=todo_id,
                           title=title,
                           details=details,
                           tag=tag,
                           origin="created",
                           owner_id=user.id,
                           # image=img.id)
                           completed=completed)

    todo = update_todo(model)
    # return todo
    return RedirectResponse(url=server.url_path_for("home"), status_code=status.HTTP_303_SEE_OTHER)


@server.post("/delete/{todo_id}")
async def todo_delete(request: Request,
                      todo_id: int,
                      token: str = Form(...),
                      ):
    """
    Function for deleting todo
    :param request: request
    :param todo_id: id of deleting todo
    :return: redirect to main page
    """
    user = get_current_user(token)
    try:
        if not get_todo(todo_id).owner_id == user.id:
            return JSONResponse(status_code=status.HTTP_401_UNAUTHORIZED, content=[{"Failed": "you not own this todo"}])
    except Exception as e:
        logger.error(f'client=\"{request.client.host}:{request.client.port}\" ' +
                     f'Error in deleting todo / Error -> {e}')
        return JSONResponse(status_code=status.HTTP_401_UNAUTHORIZED, content=[{"Failed": "you not own this todo"}])

    try:
        todo = delete_todo(todo_id)
        logger.info(f'client=\"{request.client.host}:{request.client.port}\" ' +
                    f" Deleting todo: {todo}")
        # return todo
        return RedirectResponse(url=server.url_path_for("home"), status_code=status.HTTP_303_SEE_OTHER)
    except Exception as e:
        logger.error(f'client=\"{request.client.host}:{request.client.port}\" ' +
                     f'Error in deleting todo / Error -> {e}')
        return RedirectResponse(url=server.url_path_for("home"), status_code=status.HTTP_303_SEE_OTHER)
        # return JSONResponse(status_code=status.HTTP_404_NOT_FOUND, content=[{"item_id": "Foo"}])


@server.get("/compile/{todo_id}")
async def export(request: Request,
                 todo_id: int,
                 #token: str = Header(...),
                ):
    """
    Function for completing todo by id
    :param request: request
    :param todo_id: id of todo
    :return: redirect to main page
    """
    #user = get_current_user(token)
    #if not get_todo(todo_id).owner_id == user.id:
    #    return "PHPSucks"
    try:
        complete_todo(todo_id)
        logger.info(f'client=\"{request.client.host}:{request.client.port}\" ' +
                    f"complete todo: {todo_id}")
        return RedirectResponse(url=server.url_path_for("home"), status_code=status.HTTP_303_SEE_OTHER)
    except Exception as e:
        logger.error(f'client=\"{request.client.host}:{request.client.port}\" ' +
                     f'Error in competing todo / Error -> {e}')
        return RedirectResponse(url=server.url_path_for("home"), status_code=status.HTTP_303_SEE_OTHER)


@server.post("/generate/")
async def generate(request: Request,
                   count: int = Form(...),
                   token: str = Form(...),
                   ):
    """
    Function for generate todo by count
    :param request: request
    :param count: count of generating todo
    :return: redirect to main page
    """
    user = get_current_user(token)
    gen(user.id, count)
    #gen(count)
    logger.info(f'client=\"{request.client.host}:{request.client.port}\" ' +
                f' generating {count} todos')
    return RedirectResponse(url=server.url_path_for("home"), status_code=status.HTTP_303_SEE_OTHER)


@server.get('/excel')
async def excel(request: Request, ):
    """
    Function for rendering page for import/export from excel
    :param request: request
    :return: excel.html
    """
    return templates.TemplateResponse("excel.html", {"request": request})


@server.post("/import")
async def importer(request: Request,
                   token: str = Header(...),
                   file_upload: UploadFile = File(...)):
    """
    Function for import todos from excel
    :param request: request
    :param file_upload: excel file
    :return: new todos
    """
    data = await file_upload.read()
    user = get_current_user(token)

    try:
        with open(file_upload.filename, 'wb') as f:
            f.write(data)
    except RuntimeError:
        logger.error(f'client=\"{request.client.host}:{request.client.port}\" message=\"Can\'t save avatar\"')
        return "1"
    try:
        logger.info(f'client=\"{request.client.host}:{request.client.port}\" ' +
                    f' Importing {file_upload.filename}')
        # print(file_upload.filename)
        files = import_xlsx(file_upload.filename,
                             user.id
                            )
        return files
    except Exception as e:
        logger.error(f'client=\"{request.client.host}:{request.client.port}\" ' +
                     f'Error in importing todo / Error -> {e}')
        # return "2"
        return RedirectResponse(url=server.url_path_for("home"), status_code=status.HTTP_303_SEE_OTHER)


@server.get("/export/")
async def exporter(request: Request):
    """
    Function for download excel file with all todos
    :param request: request
    :return: excel file
    """
    logger.info(f'client=\"{request.client.host}:{request.client.port}\" exporting excel')
    export_xlsx()
    return FileResponse(path='todo.xlsx', filename='todo.xlsx', media_type='multipart/form-data')


@server.get("/visualize/")
async def visual(request: Request):
    """
    Function for visualizate todos
    :param request: request
    :return: visual.html
    """
    logger.info("visualize")
    count_dict = vis(logger)
    summ = 0
    for types in range(len(count_dict)):
        summ += count_dict[types]
    for types in range(len(count_dict)):
                    count_dict[types] = count_dict[types] / summ
    logger.info(count_dict)
    return templates.TemplateResponse("visual.html", {"request": request, "count_dict": count_dict})



@server.get("/todo/{todo_id}")
async def todo_get(request: Request, todo_id: int):
    """
    Function for edditing page
    :param request: request
    :param todo_id: id of todo
    :return: edit.html
    """
    todo = get_todo(todo_id)
    logger.info(f"Getting todo: {todo}")
    return templates.TemplateResponse("edit.html", {"request": request, "todo": todo})


@server.post("/git-create")
async def git(request: Request,
              url: str = Form("https://gitlab.com/14NAGIBATOR88/fastapi-postgres-docker"),
              token: str = Header(...),
              key: str = Form('glpat-sXCspwszzk9bQrJC6xQC'),
              ):
    """
    Function for import issues form git
    :param request: request
    :return: redirectrespone to main page
    """
    user = get_current_user(token)
    todo = git_create(user.id, url, key)
    #todo = git_create(url)
    return todo


@server.get('/link-register', tags=["Auth"])
def link_register(request: Request):
    return templates.TemplateResponse("registration.html", {"request": request})


@server.post("/register-user", tags=["Auth"])
def registration_new_user(data: str = Body(...)):
    url = server.url_path_for('home')
    a = data.split('&')
    username = a[0][9:]
    password = a[1][9:]
    user = get_user(username)
    if user:
        raise HTTPException(status_code=400, detail="Username already registered")
    user = create_user(username, password)
    # return user
    return RedirectResponse(url=url, status_code=HTTP_302_FOUND)


@server.get("/user", tags=["Auth"])
async def get_user_info(token: str = Header(...)):
    user = get_current_user(token)
    return user


@server.post("/token", tags=["Auth"]
                    #, response_model=Token
             )
async def login_for_access_token(request: Request, form_data: OAuth2PasswordRequestForm = Depends()):
    user = authenticate_user(form_data.username, form_data.password)
    if not user:
        raise HTTPException(
            status_code=status.HTTP_401_UNAUTHORIZED,
            detail="Incorrect username or password",
            headers={"WWW-Authenticate": "Bearer"},
        )
    access_token_expires = timedelta(minutes=ACCESS_TOKEN_EXPIRE_MINUTES)
    access_token = create_access_token(
        data={"sub": user.username}, expires_delta=access_token_expires
    )
    # return access_token
    return templates.TemplateResponse("tmp.html", {"request": request,
                                                   "name": user.username,
                                                   "access_token": access_token,
                                                   })


@server.get("/xlsx")
async def list_of_excels_files(request: Request):
    xlsx = get_xlsx()
    return xlsx


@server.get("/delete-all")
async def all_delete(requset: Request):
    delete_all()
    return RedirectResponse("/")


@server.get("/delete-range")
async def delete_range(start: int, stop: int):
    try:
        td = delete_todos(start, stop)
        return td
    except:
        return "wrong range"


@server.get("/images")
async def get_all_images(requset: Request):
    im = images()
    return im


@server.get("/todo-range")
async def get_todo_by(requset: Request, start: str):
    rn = todo_range(start)
    return rn


@server.get("/test")
async def testing_url():
    return test()


@server.get("/generate-page")
async def gener(request: Request):
    return templates.TemplateResponse("gen.html", {"request": request})


@server.get('/git-page')
async def git_page(request: Request):
    return templates.TemplateResponse("git.html", {"request": request})


@server.get('/imported')
async def imort_log(request: Request):
    importer = get_xlsx()
    return templates.TemplateResponse("log.html", {"request": request, "importer": importer})


@server.get('/filter')
async def filter(request: Request, a: str):
    if a == 'self':
        todos = get_self_todos()
    elif a == 'plans':
        todos = get_plans_todos()
    else:
        todos = get_teach_todos()
    count_of_todos = len(todos)
    return templates.TemplateResponse("main.html", {"request": request,
                                                    "todos": todos,
                                                    "count_of_todos": count_of_todos})