async function fetchUser() {
    const response = await fetch("/user", {
        method: "GET",
        headers: {
            "Content-Type": "application/json",
            "token": token,
        },
    })
    const jsonData = await response.json();
    const username = jsonData.username;
    if ( username == undefined) {
        localStorage.removeItem('token');
        window.location.href="/";
    }
    document.getElementById('loginingUsername').innerHTML = username;
}

async function exit() {
    localStorage.removeItem('token');
    window.location.href = '/';
}