from datetime import datetime
from enum import Enum
from typing import Optional

from pydantic import BaseModel, constr


class Tag(str, Enum):
    studies = "studies"
    personal = "personal"
    plans = "plans"
    empty = "empty"


class Origin(str, Enum):
    generated = "generated"
    created = "created"


class CreateTodo(BaseModel):
    title: constr(max_length=500)
    details: Optional[str] = None
    tag: Tag
    origin: Origin
    owner_id: int
    completed: Optional[bool] = False
    completed_date: Optional[str] = None
    created_date: Optional[str] = datetime.now().strftime('%Y-%m-%d')
    image_id: Optional[int]

    class Config:
        use_enum_values = True


class UpdateTodo(BaseModel):
    id: int
    title: constr(max_length=500)
    details: constr(max_length=500)
    tag: Tag
    completed: bool
    image_id: Optional[int]


class CreateImage(BaseModel):
    path: str
    hashed_image: str
